-- import user settings files
require("user.plugins-setup") -- this should be at the top

-- import the core files
require("user.core.settings")
require("user.core.colorscheme")
require("user.core.keymaps")

-- plugins
require("user.plugins.comment")
require("user.plugins.nvim-tree")
require("user.plugins.lualine")
require("user.plugins.telescope")
require("user.plugins.nvim-cmp")
require("user.plugins.lsp.mason")
require("user.plugins.lsp.lspsaga")
require("user.plugins.lsp.lspconfig")
require("user.plugins.autopairs")
require("user.plugins.treesitter")
require("user.plugins.gitsigns")
require("user.plugins.knap")
-- require("user.plugins.vimtex")
