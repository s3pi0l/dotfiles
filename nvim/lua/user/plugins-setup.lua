local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
        vim.cmd([[packadd packer.nvim]])
        return true
    end
    return false
end
local packer_bootstrap = ensure_packer() -- true if packer was just installed

-- Autocommand that reloads neovim whenever you save this file
vim.cmd([[
    augroup packer_user_config
        autocmd!
        autocmd BufWritePost plugins-setup.lua source <afile> | PackerSync
    augroup end
]])

local status, packer = pcall(require, "packer")
if not status then
    return
end

return packer.startup(function(use)
    -- setup your favourite plugins here
    use("wbthomason/packer.nvim") -- packer itself
    use("nvim-lua/plenary.nvim") -- lua functions that many plugins use
    
    use("folke/tokyonight.nvim") -- colorscheme
    use("bluz71/vim-nightfly-guicolors") -- colorscheme
    use("rebelot/kanagawa.nvim")

   
    use("tpope/vim-surround") -- surround code with parantehses, quotes, etc.
    use("numToStr/Comment.nvim") -- commenting with gc
    use("nvim-tree/nvim-tree.lua") -- file explorer
    use("kyazdani42/nvim-web-devicons") -- icons!
    use("nvim-lualine/lualine.nvim")

    use({ "nvim-telescope/telescope-fzf-native.nvim", run = "make" })
    use("nvim-telescope/telescope.nvim")

    use("hrsh7th/nvim-cmp") -- autocompletion helper
    use("hrsh7th/cmp-buffer") -- autocompletion helper
    use("hrsh7th/cmp-path") --autocompletion helper

    --snippets
    use("L3MON4D3/LuaSnip")
    use("saadparwaiz1/cmp_luasnip")
    use("rafamadriz/friendly-snippets")
    
    use("williamboman/mason.nvim") -- manage the lsp servers
    use("williamboman/mason-lspconfig.nvim") -- glue between mason and lsp
    use("neovim/nvim-lspconfig") -- configure the lsp servers

    use("hrsh7th/cmp-nvim-lsp")
    use("glepnir/lspsaga.nvim")
    use("onsails/lspkind.nvim")
    
    -- treesitter 
    use({
        "nvim-treesitter/nvim-treesitter",
        run = function()
            require("nvim-treesitter.install").update({with_sync = true})
        end,
    })
    
    -- tools for autoapirs
    use("windwp/nvim-autopairs")
    
    -- git integration
    use("lewis6991/gitsigns.nvim")

    -- latex live preview
    use("frabjous/knap")
    

    if packer_bootstrap then
        require("packer").sync()
    end
 end)
