local status, lualine = pcall(require, "lualine")
if not status then
    return
end

-- local lualine_onedark = require("lualine.themes.onedark")
-- lualine.setup({
--     options = {
--         theme = lualine_onedark,
--         -- section_separators = '',
--         -- component_separators = ''
--     }
-- })

require('lualine').setup {
    options = {
        theme = 'tokyonight',
        section_separators = '',
        component_separators = ''
    }
}
