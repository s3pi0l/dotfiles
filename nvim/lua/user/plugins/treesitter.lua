local status, treesitter = pcall(require, "nvim-treesitter.configs")
if not status then
    return
end

treesitter.setup({
    highlight = {
        enable = true,
    },
    indent = { enable = true },
    autotag = { enable = true },
    ensure_installed = {
        "markdown",
        "json",
        "lua",
        "latex",
        "c",
        "python",
        "rust",
        "go",
    },
    auto_install = true,
})
