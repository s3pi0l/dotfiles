--local status, _ = pcall(vim.cmd, "colorscheme tokyonight-night")
--if not status then
--    print("Colorscheme: Tokyonight not found!")
--    return
--end

-- local status, _ = pcall(vim.cmd, "colorscheme nightfly")
-- if not status then
--     print("Colorscheme: Nightfly not found!")
--     return
-- end

-- local status, _ = pcall(vim.cmd, "colorscheme onedark")
-- if not status then
--     print("colorscheme: Onedark not found!")
--     return
-- end

local status, _ = pcall(vim.cmd, "colorscheme kanagawa")
if not status then
    print("colorscheme: Kanagawa not found!")
    return
end
