vim.g.mapleader = " "

local keymap = vim.keymap -- for conciseness

-- the format for the keymaps are: keymap.set("<mode>", "<keys>", "<action>")

-- general keymaps
keymap.set("n", "x", '"_x') -- don't store the single characters cut in normal mode to any register
keymap.set("n", "<leader>+", "<C-a>") -- increment in normal mode
keymap.set("n", "<leader>-", "<C-x>") -- decrement in normal mode

-- nvim-tree
keymap.set("n", "<leader>e", ":NvimTreeToggle<CR>")
